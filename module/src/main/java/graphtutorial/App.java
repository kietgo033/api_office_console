// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

package graph;

import java.io.IOException;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;
import com.microsoft.graph.models.User;

/**
 * Graph Tutorial
 */
public class App {
    public static void main(String[] args) {

        final Properties oAuthProperties = new Properties();
        try {
            oAuthProperties.load(App.class.getClassLoader().getResourceAsStream("oAuth.properties"));
        } catch (IOException e) {
            System.out.println("Unable to read OAuth configuration. Make sure you have a properly formatted oAuth.properties file.");
            return;
        }

        final String appId = oAuthProperties.getProperty("app.id");
        final List<String> appScopes = Arrays
                .asList(oAuthProperties.getProperty("app.scopes").split(","));

        Graph.initializeGraphAuth(appId, appScopes);
        final String accessToken = Graph.getUserAccessToken();


        // Greet the user
        User me = Graph.getMe();
        System.out.println("Welcome " + me.displayName);
        System.out.println();

        Scanner input = new Scanner(System.in);
        int choice = -1;
        while (choice != 0) {
            System.out.println("Please choose one of the following options:");
            System.out.println("0. Exit");
            System.out.println("1. Display access token");
            System.out.println("2. Create User");
            System.out.println("3. Delete User");
            //System.out.println("3. Assign Role.. Khong dung nua");
            System.out.println("4. Get list user");
            System.out.println("5. Restore User");
            //System.out.println("5. Get list role of user.. Khong dung nua");
            System.out.println("6. Get license of user");
            System.out.println("7. Assign license for user");
            System.out.println("8. Get list role activated");
            System.out.println("9. Assign role for user by id of Role(Khong dung)");
            System.out.println("10. Get role by tempalteId");
            System.out.println("11. Assign role for user by tempalteId");
            System.out.println("12. Get all roles template");
            System.out.println("13. Get list Deleted user");
            System.out.println("14. Get list Domains");
            System.out.println("15. Create domain");
            System.out.println("16. Verify domain");
            System.out.println("17. List verify dns record");
            System.out.println("18. List service Configuration Records ");
            System.out.println("19. Activate RoleTemplate");
            System.out.println("20. Get list members of Role by use templateId");
            System.out.println("21. List groups");
            System.out.println("22. Create group Microsoft 365");
            System.out.println("23. Create group Security");
            System.out.println("24. Delete group");
            System.out.println("25. List deleted group");
            System.out.println("26. Restore group");
            System.out.println("27. Update group");
            System.out.println("28. Validate mailNickname");
            System.out.println("29. Update group Security");
            System.out.println("30. Create contact");
            System.out.println("31. List contact");
            System.out.println("32. Delete contact");
            System.out.println("33. Update contact");

            try {
                System.out.print("Your choose: ");
                choice = input.nextInt();
            } catch (InputMismatchException ex) {
                ex.printStackTrace();
            }
            input.nextLine();
            switch (choice) {
                case 0:
                    // Exit the program
                    System.out.println("Goodbye...");
                    break;
                case 1:
                    // Display access token
                    System.out.println("Access token: " + accessToken);
                    break;
                case 2:
                    // Create user
                    Graph.createUser();
                    break;
                case 3:
                    Graph.deleteUser();
                    break;
                case 4:
                    Graph.getUsers();
                    break;
                case 5:
                    //Graph.listAppRole();
                    Graph.restoreUser();
                    break;
                case 6:
                    Graph.getLicenseUser();
                    break;
                case 7:
                    Graph.assignLicense();
                    break;
                case 8:
                    Graph.getListRoleActivated();
                    break;
                case 9:
                    Graph.assignRoletoUserByIdOfRole();
                    break;
                case 10:
                    Graph.getRoleByTemplateId();
                    break;
                case 11:
                    Graph.assignRoletoUserByTempalteId();
                    break;
                case 12:
                    Graph.getListRoleTemplate();
                    break;
                case 13:
                    Graph.getDeletedUserList();
                    break;
                case 14:
                    Graph.listDomains();
                    break;
                case 15:
                    Graph.createDomain();
                    break;
                case 16:
                    Graph.verifyDomain();
                    break;
                case 17:
                    Graph.listVerifyDns();
                    break;
                case 18:
                    Graph.listServiceConfigurationRecords();
                    break;
                case 19:
                    Graph.activateRoleTemplate();
                    break;
                case 20:
                    Graph.listMembersOfRole();
                    break;
                case 21:
                    Graph.listGroups(0);
                    break;
                case 22:
                    Graph.createGroupMicrosoft365();
                    break;
                case 23:
                    Graph.createGroupSecurity();
                    break;
                case 24:
                    Graph.deleteGroup();
                    break;
                case 25:
                    Graph.listDeletedGroup();
                    break;
                case 26:
                    Graph.restoreGroup();
                    break;
                case 27:
                    Graph.updateGroup();
                    break;
                case 28:
                    Graph.validateMailNickname("Group");
                    break;
                case 29:
                    Graph.updateGroupSecurity();
                    break;
                case 30:
                    Graph.createContact();
                    break;
                case 31:
                    Graph.listContact();
                    break;
                case 32:
                    Graph.deleteContact();
                    break;
                case 33:
                    Graph.updateContact();
                    break;
                default:
                    System.out.println("Invalid choice");
            }
        }
        input.close();
    }


}