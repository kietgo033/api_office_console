// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT license.

package graph;

import java.net.URL;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.regex.Pattern;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.microsoft.graph.models.*;
import com.microsoft.graph.options.HeaderOption;
import com.microsoft.graph.options.Option;
import com.microsoft.graph.requests.*;
import okhttp3.Request;

import com.azure.identity.DeviceCodeCredential;
import com.azure.identity.DeviceCodeCredentialBuilder;

import com.microsoft.graph.authentication.TokenCredentialAuthProvider;
import com.microsoft.graph.logger.DefaultLogger;
import com.microsoft.graph.logger.LoggerLevel;
import org.omg.CORBA.PUBLIC_MEMBER;

/**
 * Graph
 */
public class Graph {

    private static GraphServiceClient<Request> graphClient = null;
    private static TokenCredentialAuthProvider authProvider = null;
    public static Scanner sc = new Scanner(System.in);

    public static void initializeGraphAuth(String applicationId, List<String> scopes) {
        // Create the auth provider
        final DeviceCodeCredential credential = new DeviceCodeCredentialBuilder()
                .clientId(applicationId)
                .tenantId("common") //c425e655-971c-424f-8164-0f9616d8e636
                .challengeConsumer(challenge -> System.out.println(challenge.getMessage()))
                .build();

        authProvider = new TokenCredentialAuthProvider(scopes, credential);

        // Create default logger to only log errors
        DefaultLogger logger = new DefaultLogger();
        logger.setLoggingLevel(LoggerLevel.ERROR);

        // Build a Graph client
        graphClient = GraphServiceClient.builder()
                .authenticationProvider(authProvider)
                .logger(logger)
                .buildClient();
    }

    public static String getUserAccessToken() {
        try {
            URL meUrl = new URL("https://graph.microsoft.com/v1.0/me");
            return authProvider.getAuthorizationTokenAsync(meUrl).get();
        } catch (Exception ex) {
            return null;
        }
    }

    // <GetUserSnippet>
    public static User getMe() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");

        // GET /me to get authenticated user
        User me = graphClient
                .me()
                .buildRequest()
                .get();
        /*
        LicenseDetailsCollectionPage licenseDetails = graphClient.me().licenseDetails()
                .buildRequest()
                .get();
        List<LicenseDetails> licenseDetailList=licenseDetails.getCurrentPage();
        for(LicenseDetails l: licenseDetailList){
            System.out.println("Id: "+l.id);
            System.out.println("oDataType: "+l.oDataType);
            System.out.println("skuPartNumber: "+l.skuPartNumber);
            System.out.println("SkuId: "+l.skuId);
            int i=1;
            for (ServicePlanInfo s: l.servicePlans){
                System.out.println("STT: "+i);
                System.out.println("appliesTo: "+s.appliesTo);
                System.out.println("oDataType: "+s.oDataType);
                System.out.println("provisioningStatus: "+s.provisioningStatus);
                System.out.println("servicePlanName: "+s.servicePlanName);
                System.out.println("servicePlanId: "+s.servicePlanId);
                i++;
            }
        }
         */
        return me;
    }

    public static void getUser(String id) {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");
        User user = graphClient
                .users(id)
                .buildRequest()
                .get();
        System.out.println("DisplayName: " + user.displayName);
        System.out.println("UserPrincipalName: " + user.userPrincipalName);
        System.out.println("Id: " + user.id);
        System.out.println("----------------------------------");
    }

    public static void getLicenseUser() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");

        getUsers();
        System.out.print("Nhap id/principalName cua user: ");
        String id = sc.nextLine();

        LicenseDetailsCollectionPage licenseDetails = graphClient.users(id).licenseDetails()
                .buildRequest()
                .get();
        List<LicenseDetails> licenseDetailList = licenseDetails.getCurrentPage();
        int stt = 1;
        for (LicenseDetails l : licenseDetailList) {
            System.out.println("STT: " + stt);
            System.out.println("Id: " + l.id);
            System.out.println("skuPartNumber: " + l.skuPartNumber);
            System.out.println("SkuId: " + l.skuId);
            stt++;
            int i = 1;
            for (ServicePlanInfo s : l.servicePlans) {
                if (s.provisioningStatus.equals("Success")) {
                    System.out.println("STT_servicePlans: " + i);
                    System.out.println("appliesTo: " + s.appliesTo);
                    System.out.println("servicePlanName: " + s.servicePlanName);
                    System.out.println("servicePlanId: " + s.servicePlanId);
                    i++;
                }
            }
            System.out.println("---------------------------");
        }
    }

    public static void getUsers() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");

        UserCollectionPage users = graphClient.users()
                .buildRequest()
                .get();
        // co the dung them .top(int) de theo so luong.
        // mac dinh lay toi da 100
        //UserDeltaCollectionPage delta = graphClient.users()
        //	.delta()
        //	.buildRequest()
        //	.select("displayName,jobTitle,mobilePhone")
        //	.get();
        // cu phap de lay cac cac thuoc tinh nhu tren

        List<User> userList = users.getCurrentPage();
        int i = 1;
        for (User u : userList) {
            System.out.println("STT: " + i);
            System.out.println("Name: " + u.displayName);
            System.out.println("ID: " + u.id);

            //System.out.println("Role: "+u.appRoleAssignments.toString());
            System.out.println("UserName: " + u.userPrincipalName);
            //System.out.println("License: "+u.licenseDetails.toString());
            i++;
        }


        //Neu co rat nhieu user (>100) thi ta dung cach sau:
        /*
        List<List<User>> allUserList = new ArrayList<>();

        UserCollectionPage users = graphClient.users()
                .buildRequest()
                .get();

        do {
            List<User> currentPageUser = users.getCurrentPage();
                        Collections.addAll(allUserList, currentPageUser);
                        UserCollectionRequestBuilder nextPage = users.getNextPage();
            users = nextPage == null ? null : nextPage.buildRequest().get();
        } while (users != null);

        return allUserList;
         */
    }

    public static void getAllUsersWithDisplayNameAndUserPrincipalName() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");
        UserCollectionPage users = graphClient.users()
                .buildRequest()
                .select("displayName,userPrincipalName")
                .get();
        List<User> userList = users.getCurrentPage();
        int i = 1;
        for (User u : userList) {
            System.out.println("STT: " + i);
            System.out.println("Name: " + u.displayName);
            System.out.println("UserName: " + u.userPrincipalName);
            i++;
        }
    }

    public static String getUserPrincipalNameByIdWithDeletedUser(String id) {
        DirectoryObject directoryObject = graphClient.directory().deletedItems(id)
                .buildRequest()
                .select("userPrincipalName")
                .get();
        return ((User) directoryObject).userPrincipalName.replaceAll(id.replaceAll("-", ""), "");
    }

    public static void getDeletedUserList() {
        LinkedList<Option> requestOptions = new LinkedList<Option>();
        requestOptions.add(new HeaderOption("ConsistencyLevel", "eventual"));
        UserCollectionPage userList = graphClient.directory().deletedItemsAsUser()
                .buildRequest(requestOptions)
                .select("id,displayName,deletedDateTime")
                .orderBy("displayName desc") // asc and desc
                .get();
        System.out.println("DANH SACH CAC USER DA BI XOA");
        int i = 1;
        for (User u : userList.getCurrentPage()) {
            System.out.println("STT: " + i);
            System.out.println("Id: " + u.id);
            System.out.println("DisplayName: " + u.displayName);
            //"Asia/Ho_Chi_Minh" "UTC+07" la cac ZoneId
            //dung phuong thuc atZoneSameInstant(ZoneId.of("Asia/Ho_Chi_Minh") cua java.time.OffsetDateTime
            System.out.println("UserPrincipalName: " + getUserPrincipalNameByIdWithDeletedUser(u.id));
            //System.out.println("Delete Time: "+u.deletedDateTime.atZoneSameInstant(ZoneId.of("UTC+07")).toLocalTime()+" "+u.deletedDateTime.atZoneSameInstant(ZoneId.of("UTC+07")).toLocalDate().format(DateTimeFormatter.ofPattern("dd/MM/YYYY")));
            System.out.println("Delete Time: " + u.deletedDateTime.atZoneSameInstant(ZoneId.of("UTC+07")).format(DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm:ss")));
            i++;
        }
    }

    public static String getTime(OffsetDateTime offsetDateTime) {
        return offsetDateTime.atZoneSameInstant(ZoneId.of("UTC+07")).format(DateTimeFormatter.ofPattern("dd/MM/YYYY HH:mm:ss"));
    }

    public static void restoreUser() {
        getDeletedUserList();
        System.out.print("Nhap id user can restore: ");
        String id = sc.nextLine();
        try {
            graphClient.directory().deletedItems(id)
                    .restore()
                    .buildRequest()
                    .post();
            System.out.println("Khoi phuc thanh cong!");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Khoi phuc that bai!");
        }
    }

    public static void assignLicense() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");

        try {
            LinkedList<AssignedLicense> addLicensesList = new LinkedList<AssignedLicense>();
            AssignedLicense addLicenses = new AssignedLicense();

//        LinkedList<UUID> disabledPlansList = new LinkedList<UUID>();
//        disabledPlansList.add(UUID.fromString("57ff2da0-773e-42df-b2af-ffb7a2317929"));
//        disabledPlansList.add(UUID.fromString("094e7854-93fc-4d55-b2c0-3ab5369ebdc1"));
//        disabledPlansList.add(UUID.fromString("e95bec33-7c88-4a70-8e19-b10bd9d0c014"));
//        disabledPlansList.add(UUID.fromString("c7699d2e-19aa-44de-8edf-1736da088ca1"));
//        addLicenses.disabledPlans = disabledPlansList;

            // id cua License
            //hien chua biet cach lay cac license co trong app..
            //phai xem license cua user moi biet duoc...
            addLicenses.skuId = UUID.fromString("f245ecc8-75af-4f8e-b61f-27d8114de5f3");
            addLicensesList.add(addLicenses);
            getUsers();
            System.out.print("Nhap id/principalName cua user de gan license: ");
            String id = sc.nextLine();

            LinkedList<UUID> removeLicensesList = new LinkedList<UUID>();
            graphClient.users(id)
                    .assignLicense(UserAssignLicenseParameterSet
                            .newBuilder()
                            .withAddLicenses(addLicensesList)
                            .withRemoveLicenses(removeLicensesList)
                            .build())
                    .buildRequest()
                    .post();
            System.out.println("Assign license successful");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Co loi xay ra");
        }
    }

    public static void createUser() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");
        String random = random();
        User user = new User();
        System.out.println("--------TAO USER---------");
        user.usageLocation = "VN";
        //user.country="Vietnam";
        System.out.print("First name: ");
        user.givenName = sc.nextLine();
        System.out.print("Last name: ");
        user.surname = sc.nextLine();
        //System.out.print("Display name: "+user.surname+" "+user.givenName);
        System.out.print("Display name: ");
        user.displayName = sc.nextLine();
        System.out.print("Username: ");
        user.userPrincipalName = sc.nextLine();
        user.mailNickname = user.userPrincipalName;

        /*
        user.jobTitle ;
        user.department;
        user.officeLocation;
        user.businessPhones; //(list)
        user.faxNumber;
        user.mobilePhone;
        user.streetAddress;
        user.city;
        user.state;
        user.postalCode;
        user.country;
         */

        listDomainsVerified();
        System.out.print("Chon Domain: ");
        String domain = sc.nextLine();
        domain = "@" + domain;
        user.userPrincipalName += domain;
        //kiem tra userPrincipalName co ton tai hay khong??

        user.accountEnabled = true;
        //user.displayName = "Kiet Phan_" + random;
        //user.userPrincipalName = "kietgo" + random + "@lvsoffice.cf";
        PasswordProfile passwordProfile = new PasswordProfile();
        passwordProfile.forceChangePasswordNextSignIn = false; // false thi la khong tick, true la tick: Require this user to change their password when they first sign in
        passwordProfile.password = "xWwvJ]6sss" + random;// tu dong tao mat khau
        user.passwordProfile = passwordProfile;
        System.out.print("Password: " + user.passwordProfile.password);

        try {
            graphClient.users().buildRequest().post(user);
            System.out.println("\nTao thanh cong");
        } catch (Exception e) {
            System.out.println("\nTao that bai");
            e.printStackTrace();
        }
        // Tao thanh cong thi se lam cac buoc tiep theo:
        //Gan license: assignLicense()
        //Gan role: assignRoletoUserByTempalteId()
    }

    public static void createDomain() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");
        System.out.print("Nhap domain muon tao: ");
        String domainId = sc.nextLine();
        Domain domain = new Domain();
        domain.id = domainId;
        try {
            graphClient.domains().buildRequest().post(domain);
            System.out.println("Tao thanh cong");
        } catch (Exception e) {
            System.out.println("Tao that bai");
            e.printStackTrace();
        }
    }

    public static void verifyDomain() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");
        listDomains();
        System.out.print("Nhap domain muon xac minh(Chi cac domain co isVerified:false): ");
        String domainId = sc.nextLine();
        try {
            graphClient.domains(domainId)
                    .verify()
                    .buildRequest()
                    .post();
            System.out.println("Xac minh thanh cong thanh cong");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Xac minh that bai");
        }
    }

    public static void listVerifyDns() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");
        listDomains();
        System.out.print("Nhap domain muon xac minh (Chi cac domain co isVerified:false): ");
        String domainId = sc.nextLine();
        System.out.print("Nhap loai Dns record (TXT/MX): ");
        String type = sc.nextLine();
        try {
            DomainDnsRecordCollectionPage verificationDnsRecords = graphClient.domains(domainId).verificationDnsRecords()
                    .buildRequest()
                    .get();
            for (DomainDnsRecord domainDnsRecord : verificationDnsRecords.getCurrentPage()) {
                if (type.equalsIgnoreCase("txt")) {
                    System.out.println("Type: " + domainDnsRecord.recordType);
                    System.out.println("Name: @ (or skip if not supported by provider)");
                    System.out.println("TXT value: " + ((DomainDnsTxtRecord) domainDnsRecord).text);
                    System.out.println("TTL: " + domainDnsRecord.ttl);
                    break;
                }

                if (type.equalsIgnoreCase(domainDnsRecord.recordType)) {
                    System.out.println("Type: " + domainDnsRecord.recordType);
                    System.out.println("Host Name: @ (or skip if not supported by provider)");
                    System.out.println("Points to address or value: " + ((DomainDnsMxRecord) domainDnsRecord).mailExchange);
                    System.out.println("Priority: " + ((DomainDnsMxRecord) domainDnsRecord).preference);
                    System.out.println("TTL: " + domainDnsRecord.ttl);
                    break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void listServiceConfigurationRecords() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");
        listDomains();
        System.out.print("Nhap domain muon xac minh (Chi cac domain co isVerified:false): ");
        String domainId = sc.nextLine();
        try {
            DomainDnsRecordCollectionPage serviceConfigurationRecords = graphClient.domains(domainId).serviceConfigurationRecords()
                    .buildRequest()
                    .get();


            for (DomainDnsRecord domainDnsRecord : serviceConfigurationRecords.getCurrentPage()) {
                if (domainDnsRecord.recordType.equalsIgnoreCase("Mx")) {
                    System.out.println("Type: " + domainDnsRecord.recordType);
                    System.out.println("Host Name: @ (or skip if not supported by provider)");
                    System.out.println("Points to address or value: " + ((DomainDnsMxRecord) domainDnsRecord).mailExchange);
                    System.out.println("Priority: " + ((DomainDnsMxRecord) domainDnsRecord).preference);
                    System.out.println("TTL: " + domainDnsRecord.ttl);
                    continue;
                }
                if (domainDnsRecord.recordType.equalsIgnoreCase("Txt")) {
                    System.out.println("Type: " + domainDnsRecord.recordType);
                    System.out.println("Name: @ (or skip if not supported by provider)");
                    System.out.println("Value: " + ((DomainDnsTxtRecord) domainDnsRecord).text);
                    System.out.println("TTL: " + domainDnsRecord.ttl);
                    continue;
                }
                if (domainDnsRecord.recordType.equalsIgnoreCase("Cname")) {
                    System.out.println("Type: " + domainDnsRecord.recordType);
                    System.out.println("Name: " + domainDnsRecord.label.replaceAll("." + domainId, ""));
                    System.out.println("Value: " + ((DomainDnsCnameRecord) domainDnsRecord).canonicalName);
                    System.out.println("TTL: " + domainDnsRecord.ttl);
                    continue;
                }
                System.out.println("-------------------------------------------------------");

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // day la role app.. Se khong dung nua.
    public static void assignRole() {
        if (graphClient == null) throw new NullPointerException(
                "Graph client has not been initialized. Call initializeGraphAuth before calling this method");

        RoleAssignment role = new RoleAssignment();
        AppRoleAssignment appRoleAssignment = new AppRoleAssignment();
        appRoleAssignment.principalId = UUID.fromString("c77f4ac2-902d-4e51-89b1-792f755c1092");
        appRoleAssignment.resourceId = UUID.fromString("e4958673-3601-4783-99aa-d1a9efeb7f8a");
        appRoleAssignment.appRoleId = UUID.fromString("cc6f6d51-cd0b-4569-8ac3-18057097f243");

        graphClient.users("c77f4ac2-902d-4e51-89b1-792f755c1092").appRoleAssignments()
                .buildRequest()
                .post(appRoleAssignment);
    }

    // khong dung
    public static void listAppRole() {
        //GraphServiceClient graphClient = GraphServiceClient.builder().authenticationProvider(authProvider).buildClient();

        AppRoleAssignmentCollectionPage appRoleAssignments = graphClient.users("c77f4ac2-902d-4e51-89b1-792f755c1092").appRoleAssignments()
                .buildRequest()
                .get();
        List<AppRoleAssignment> roleList = appRoleAssignments.getCurrentPage();

        for (AppRoleAssignment a : roleList) {
            System.out.println("DisplayName: " + a.principalDisplayName);
            System.out.println("ID: " + a.principalId);
            System.out.println("ResourceDisplayName: " + a.resourceDisplayName);
            System.out.println("Resource ID: " + a.resourceId);
            System.out.println("AppRole ID: " + a.appRoleId);
        }
    }


    public static void listMembersOfRole() {
        getListRoleActivated();
        System.out.print("Nhap roleTemplateId: ");
        String roledTemplateId = sc.nextLine();
        DirectoryObjectCollectionWithReferencesPage members = graphClient.directoryRoles("roleTemplateId=" + roledTemplateId).members()
                .buildRequest()
                .get();
        for (DirectoryObject i : members.getCurrentPage()) {
            if (i.oDataType.contains("user"))
                getUser(i.id);
        }
    }

    public static void getListRoleActivated() {
        DirectoryRoleCollectionPage directoryRoles = graphClient.directoryRoles()
                .buildRequest()
                .get();
        List<DirectoryRole> directoryRoleList = directoryRoles.getCurrentPage();
        int i = 1;
        for (DirectoryRole dr : directoryRoleList) {
            System.out.println("STT: " + i);
            System.out.println("displayName: " + dr.displayName);
            System.out.println("description: " + dr.description);
            System.out.println("roleTemplateId: " + dr.roleTemplateId);
            System.out.println("id: " + dr.id);

            i++;
        }

        /*
        // co the dung id role va templateId
        // dang dung get role by id role
        System.out.println("----------role detail of role custom--------------");
        DirectoryRole directoryRole = graphClient.directoryRoles("47534b35-bae4-46b0-9378-10c8088a5a12") //sharePoint id not tempalte id
                .buildRequest()
                .get();
        System.out.println("Template Id:"+directoryRole.roleTemplateId);
        System.out.println("Desciption: "+directoryRole.description);
        System.out.println("id :"+directoryRole.id);
        System.out.println("DisplayName: "+directoryRole.displayName);
        */

        // activate role custom?... Kich hoat 1 role da kich hoat thi se bao loi...Chi dung tempalteID de kich hoat
        /*
        DirectoryRole directoryRole1 = new DirectoryRole();
        directoryRole1.roleTemplateId = "f023fd81-a637-4b56-95fd-791ac0226033";  //serive suport
        graphClient.directoryRoles()
                .buildRequest()
                .post(directoryRole1);
        */
    }

    //Luu y:
    //"Do not use - not intended for general use." -> description..ta co the loc bo cac role tempalte nay
    //"Default role for guest users. Can read a limited set of directory information."
    //"Default role for guest users with restricted access. Can read a limited set of directory information."
    //"Device Users"
    //"Default role for member users. Can read all and write a limited set of directory information."
    //"Device Join"
    //"Workplace Device Join"
    //"Only used by Azure AD Connect service."
    //"Deprecated - Do Not Use."
    //""
    //""

    public static void getListRoleTemplate() {
        DirectoryRoleTemplateCollectionPage directoryRoleTemplates = graphClient.directoryRoleTemplates()
                .buildRequest()
                .get();
        List<DirectoryRoleTemplate> list = directoryRoleTemplates.getCurrentPage();
        int i = 1;
        for (DirectoryRoleTemplate dr : list) {
            System.out.println("STT: " + i);
            System.out.println("templateId: " + dr.id);
            System.out.println("displayName: " + dr.displayName);
            System.out.println("description: " + dr.description);
            i++;
        }
    }

    public static void activateRoleTemplate() {
        getListRoleTemplate();
        System.out.print("Nhap TemplateId cua role: ");
        String templateIdRole = sc.nextLine();
        try {
            DirectoryRole directoryRole = new DirectoryRole();
            directoryRole.roleTemplateId = "fe930be7-5e62-47db-91af-98c3a49a38b1";

            graphClient.directoryRoles()
                    .buildRequest()
                    .post(directoryRole);
            System.out.print("Activate thanh cong!");
            getListRoleActivated();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.print("Activate that bai!");
        }
    }

    public static void getRoleByTemplateId() {

        System.out.print("Nhap TemplateId cua role: ");
        String templateIdRole = sc.nextLine();

        DirectoryRoleTemplate directoryRoleTemplate = graphClient.directoryRoleTemplates(templateIdRole) // secu tem id
                .buildRequest()
                .get();
        System.out.println("Desciption: " + directoryRoleTemplate.description);
        System.out.println("id :" + directoryRoleTemplate.id); // Cung chinh la id template o tren.....Tai sao lai ko co id role??
        System.out.println("DisplayName: " + directoryRoleTemplate.displayName);
    }


    public static void assignRoletoUserByIdOfRole() {
        //Co the dung id role va template id
        //Loi khi assign role da co san
        getListRoleActivated();
        System.out.print("Nhap id cua user: ");
        String id = sc.nextLine();
        System.out.print("Nhap id cua role(Role phai dc kich hoac truoc): ");
        String idRole = sc.nextLine();

        DirectoryObject directoryObject = new DirectoryObject();
        directoryObject.id = id; //c77f4ac2-902d-4e51-89b1-792f755c1092 id của user hoac direcctory object
        graphClient.directoryRoles(idRole).members().references()
                .buildRequest()
                .post(directoryObject);
    }

    public static void deleteUser() {
        String user = "";
        getUsers();
        try {
            GraphServiceClient graphClient = GraphServiceClient.builder().authenticationProvider(authProvider).buildClient();
            System.out.println("Nhap user can xoa(PrincipalName/Id): ");
            user = sc.nextLine();
            graphClient.users(user)
                    .buildRequest()
                    .delete();
            System.out.println("Da xoa thanh cong");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Co loi khi xoa user: " + user);
        }
    }

    public static String getIdbyUserPrincipalName(String userPrincipalName) {
        User user = graphClient.users(userPrincipalName)
                .buildRequest()
                .select("id")
                .get();
        return user.id;
    }

    // chi co the dung id cua user va templateIdRole
    public static void assignRoletoUserByTempalteId() {
        getUsers();
        System.out.print("Nhap userPrincipalName de gan role: ");
        String userPrincipalName = sc.nextLine();
        String idUser = getIdbyUserPrincipalName(userPrincipalName);

        getListRoleActivated();
        System.out.print("Nhap roleTemplateId: ");
        String idRoleTemplate = sc.nextLine();
        try {
            DirectoryObject directoryObject = new DirectoryObject();
            directoryObject.id = idUser; //id của user hoac direcctory object, chua test truong hop co dung duoc principalname
            graphClient.directoryRoles("roleTemplateId=" + idRoleTemplate).members().references() //secu  templatye id: 5d6b6bb7-de71-4623-b4af-96380a352509
                    .buildRequest()
                    .post(directoryObject);
            System.out.println("Da gan thanh cong!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void listDomains() {
        DomainCollectionPage domains = graphClient.domains()
                .buildRequest()
                .get();
        int i = 1;
        for (Domain d : domains.getCurrentPage()) {
            System.out.println("STT: " + i);
            System.out.println("Id: " + d.id);
            System.out.println("isVerified:" + d.isVerified);
            System.out.print("supportedServices: ");
            d.supportedServices.forEach((element) -> {
                System.out.print(element + ", ");
            });
            System.out.println();
            i++;
        }
    }

    public static void listDomainsVerified() {
        DomainCollectionPage domains = graphClient.domains()
                .buildRequest()
                .get();
        int i = 1;
        for (Domain d : domains.getCurrentPage()) {
            if (d.isVerified == true) {
                System.out.println("STT: " + i);
                System.out.println("Domain: " + d.id);
                i++;
            }
        }
    }

    ///////////////////////////////GROUP//////////////////////////////////
    // 0 thì là all, 1 thì là 365, 2 thì là group security
    public static void listGroups(int choose) {
        LinkedList<Option> requestOptions = new LinkedList<Option>();
        requestOptions.add(new HeaderOption("ConsistencyLevel", "eventual"));
        GroupCollectionPage groups = graphClient.groups()
                .buildRequest(requestOptions)
                .orderBy("displayName asc")
                .get();
        if (choose == 0) {
            int i = 1;
            for (Group g : groups.getCurrentPage()) {
                System.out.println("STT: " + i);
                System.out.println("Id: " + g.id);
                System.out.println("DisplayName: " + g.displayName);
                System.out.println("Description: " + g.description);
                System.out.println("GroupTypes: " + g.groupTypes);
                System.out.println("MailNickname: " + g.mailNickname);
                System.out.println("CreatedDateTime: " + getTime(g.createdDateTime));
                System.out.println("Mail: " + g.mail);
                System.out.println("Visibility: " + g.visibility);
                System.out.println("proxyAddress: " + g.proxyAddresses.toString());
                i++;
            }
        } else if (choose == 1) {
            int i = 1;
            for (Group g : groups.getCurrentPage()) {
                System.out.println("STT: " + i);
                System.out.println("Id: " + g.id);
                System.out.println("DisplayName: " + g.displayName);
                System.out.println("Description: " + g.description);
                System.out.println("GroupTypes: " + g.groupTypes);
                System.out.println("MailNickname: " + g.mailNickname);
                System.out.println("CreatedDateTime: " + getTime(g.createdDateTime));
                System.out.println("Mail: " + g.mail);
                System.out.println("Privacy: " + g.visibility);
                System.out.println("AllowExternalSenders: " + g.allowExternalSenders);
                System.out.println("Send copies of group conversation,..: " + g.autoSubscribeNewMembers);
                System.out.println("HideFromAddressLists: " + g.hideFromAddressLists);
                i++;
            }
        } else if (choose == 2) {
            int i = 1;
            for (Group g : groups.getCurrentPage()) {
                if (g.securityEnabled) {
                    System.out.println("STT: " + i);
                    System.out.println("Id: " + g.id);
                    System.out.println("DisplayName: " + g.displayName);
                    System.out.println("Description: " + g.description);
                    System.out.println("GroupTypes: " + g.groupTypes);
                    System.out.println("CreatedDateTime: " + getTime(g.createdDateTime));
                    System.out.println("Privacy: " + g.visibility);
                    i++;
                }
            }
        } else if (choose == 3) {
            int i = 1;
            for (Group g : groups.getCurrentPage()) {
                System.out.println("STT: " + i);
                System.out.println("Id: " + g.id);
                System.out.println("DisplayName: " + g.displayName);
                System.out.println("Mail: " + g.mail);
                i++;
            }
        }

    }

    public static void createGroupMicrosoft365() {
        //mailNickName chi duy nhat, khi tao phai kiem tra ton tai hay chua
        // mailNickName ko chua phan domain
        try {
            Group group = new Group();
            System.out.print("Nhap Name: ");
            group.displayName = sc.nextLine();
            System.out.print("Nhap Description: ");
            group.description = sc.nextLine();
            //except the following: @ () \ [] " ; : . <> , SPACE.
            System.out.print("Nhap Group email address: ");
            group.mailNickname = sc.nextLine();
            System.out.print("Chon Privacy (Public/Private): ");
            group.visibility = sc.nextLine();

            //loại nhóm mặc định của Group Microsoft365
            LinkedList<String> groupTypesList = new LinkedList<String>();
            groupTypesList.add("Unified");
            group.groupTypes = groupTypesList;
            //mặc định khi tạo trên web là sẽ được set true
            group.mailEnabled = true;
            group.securityEnabled = false;
            // trên web thì cũng chỉ mặc định là 1 email...tại đây chỉ điền mailnickname chứ ko chọn dược domain.....
            // phần này update chắc sẽ sửa được như trong center web

            // Hai lenh nay vo dung
            //group.additionalDataManager().put("\"owners@odata.bind\"", new JsonPrimitive("[  \"https://graph.microsoft.com/v1.0/users/c77f4ac2-902d-4e51-89b1-792f755c1092\"]"));
            //group.additionalDataManager().put("\"members@odata.bind\"", new JsonPrimitive("[  \"https://graph.microsoft.com/v1.0/users/38ec839a-15a3-4773-8c30-9253a7ece257\",  \"https://graph.microsoft.com/v1.0/users/b6b0cbda-01c3-4bff-a113-0cf98d6823ec\"]"));
            List<String> listOwners = new ArrayList<String>();
            List<String> listMembers = new ArrayList<String>();
            getAllUsersWithDisplayNameAndUserPrincipalName();
            String tmp = "";

            while (true) {
                System.out.print("Nhap owner(Hoac exit de thoat): "); // la cac id hoac userPrincipalName.. ta se thong nhat dung usrPrincipalName
                tmp = sc.nextLine();
                if (tmp.equalsIgnoreCase("exit") == true) {
                    break;
                }
                listOwners.add(tmp);
            }
            while (true) {
                System.out.print("Nhap members(Hoac exit de thoat): "); //// la cac id hoac userPrincipalName.. ta se thong nhat dung usrPrincipalNa
                tmp = sc.nextLine();
                if (tmp.equalsIgnoreCase("exit") == true) {
                    break;
                }
                listMembers.add(tmp);
            }
            //Khi đã có tất cả dữ liệu, thì ta sẽ tạo ủe trước, sau đó thêm các thanh viên và chủ vào ngay sau đó.
            graphClient.groups()
                    .buildRequest()
                    .post(group);
            // đã tạo thành công thì ta tiến hành lấy id của group vừa mới được tạo
            String groupId = getIdByMailNickNameGroup(group.mailNickname);
            // sau đó gán từng thành viên vào Group
            for (String memberUserPrincipalName : listMembers) {
                addOneMemberIntoGroup(getIdbyUserPrincipalName(memberUserPrincipalName), groupId);
            }
            // sau đó gán từng chủ sở hữu nhóm vào Group
            //vì khi tạo group ở lệnh trên thì mặc định me(ng dùng đang đăng nhập) sẽ được gán làm chủ sở hữu, nên ta sẽ giải quyết trường hợp này
            String meUserPrincipalName = getMe().userPrincipalName;
            if (listOwners.contains(meUserPrincipalName)) {
                listOwners.remove(meUserPrincipalName);
                for (String ownerUserPrincipalName : listOwners) {
                    addOneOwnerIntoGroup(getIdbyUserPrincipalName(ownerUserPrincipalName), groupId);
                }
            } else {
                for (String ownerUserPrincipalName : listOwners) {
                    addOneOwnerIntoGroup(getIdbyUserPrincipalName(ownerUserPrincipalName), groupId);
                }
                deleteOwnerGroup(groupId, getIdbyUserPrincipalName(meUserPrincipalName));//
            }

            System.out.println("Tao group thanh cong!");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Tao group that bai!");
        }
    }

    public static String getIdByMailNickNameGroup(String mailNickname) {
        GroupCollectionPage groups = graphClient.groups()
                .buildRequest()
                .select("id,mailNickname")
                .get();
        for (Group g : groups.getCurrentPage()) {
            if (g.mailNickname.equals(mailNickname))
                return g.id;
        }
        return null; // kha nang khi tao Group moi thi se khong ra truong hop nay
    }

    public static void createGroupSecurity() {
        try {
            Group group = new Group();
            System.out.print("Nhap Name: ");
            group.displayName = sc.nextLine();
            System.out.print("Nhap Description: ");
            group.description = sc.nextLine();
            LinkedList<String> groupTypesList = new LinkedList<String>();
            group.groupTypes = groupTypesList;
            //group security thì mặc định mailEnable là false
            group.mailEnabled = false;
            // mặc định khi tạo group security thì không có mailNickName và ta phải đặt mặc định cho nó
            group.mailNickname = "00000000-0000-0000-0000-000000000000";
            //security group phải được set securityEnalble là true
            group.securityEnabled = true;

            /*
            // Nang cap them viec se them owners vaf member vbaof luc khi tao group security
            List<String> listOwners=new ArrayList<String>();
            List<String> listMembers=new ArrayList<String>();
            getAllUsersWithDisplayNameAndUserPrincipalName();
            String tmp="";
            while(true){
                System.out.print("Nhap owner(Hoac exit de thoat): "); // la cac id hoac userPrincipalName.. ta se thong nhat dung usrPrincipalName
                tmp=sc.nextLine();
                if (tmp.equalsIgnoreCase("exit")==true){
                    break;
                }
                listOwners.add(tmp);
            }
            while(true){
                System.out.print("Nhap members(Hoac exit de thoat): "); //// la cac id hoac userPrincipalName.. ta se thong nhat dung usrPrincipalNa
                tmp=sc.nextLine();
                if (tmp.equalsIgnoreCase("exit")==true){
                    break;
                }
                listMembers.add(tmp);
            }
             */

            graphClient.groups()
                    .buildRequest()
                    .post(group);


            System.out.println("Tao group Security thanh cong!");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Tao group Security that bai!");
        }
    }

    public static void TestcreateGroupMicrosoft365() {
        try {
            Group group = new Group();
            group.description = "Group wisdáth designated owner and members";
            group.displayName = "Oaaaasdsaaaaaaddadperations group_Phan";
            LinkedList<String> groupTypesList = new LinkedList<String>();
            groupTypesList.add("Unified");
            group.groupTypes = groupTypesList;
            group.mailEnabled = true;
            group.mailNickname = "grsdoupTaoCodeaa";
            group.securityEnabled = false;
            //group.additionalDataManager().put("owners@odata.bind", new JsonPrimitive("https://graph.microsoft.com/v1.0/users/c77f4ac2-902d-4e51-89b1-792f755c1092"));
            //group.additionalDataManager().put("members@odata.bind", new JsonPrimitive("https://graph.microsoft.com/v1.0/users/38ec839a-15a3-4773-8c30-9253a7ece257"));

            //Map<String,JsonElement> additionalData = new HashMap<String, JsonElement>();
            //additionalData.put("owners@odata.bind", new JsonPrimitive("https://graph.microsoft.com/v1.0/users/c77f4ac2-902d-4e51-89b1-792f755c1092"));
            //additionalData.put("members@odata.bind", new JsonPrimitive("https://graph.microsoft.com/v1.0/users/38ec839a-15a3-4773-8c30-9253a7ece257"));
            //group.additionalDataManager().putAll(additionalData);

            group.additionalDataManager().put("owners@odata.bind", new JsonPrimitive("  \"https://graph.microsoft.com/v1.0/users/c77f4ac2-902d-4e51-89b1-792f755c1092\""));
            //group.additionalDataManager().put("members@odata.bind", new JsonPrimitive("[  \"https://graph.microsoft.com/v1.0/users/38ec839a-15a3-4773-8c30-9253a7ece257\",  \"https://graph.microsoft.com/v1.0/users/b6b0cbda-01c3-4bff-a113-0cf98d6823ec\"]"));

            //c77f4ac2-902d-4e51-89b1-792f755c1092
            //38ec839a-15a3-4773-8c30-9253a7ece257        b6b0cbda-01c3-4bff-a113-0cf98d6823ec
            Group t = graphClient.groups()
                    .buildRequest()
                    .post(group);

            System.out.println("Tao group thanh cong!");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Tao group that bai!");
        }
    }

    //khong thuc thi duoc du khong co loi xay ra?
    public static void addMembersIntoGroup() {
        try {
            Group group = new Group();
            group.additionalDataManager().put("\"members@odata.bind\"", new JsonPrimitive("[  \"https://graph.microsoft.com/v1.0/directoryObjects/38ec839a-15a3-4773-8c30-9253a7ece257\",  \"https://graph.microsoft.com/v1.0/directoryObjects/b6b0cbda-01c3-4bff-a113-0cf98d6823ec\"]"));

            graphClient.groups("052c16f7-08a0-4529-a4b2-a9c470f60454") //PHK
                    .buildRequest()
                    .patch(group);
            System.out.println("Them thanh vien vao nhom thanh cong!");

        } catch (Exception e) {
            System.out.println("Loi");
        }
    }

    public static void addOneMemberIntoGroup(String memberId, String groupId) {
        try {
            DirectoryObject directoryObject = new DirectoryObject();
            directoryObject.id = memberId;
            graphClient.groups(groupId).members().references()
                    .buildRequest()
                    .post(directoryObject);
        } catch (Exception e) {
            System.out.println("Loi addOneMemberIntoGroup()");
        }
    }

    public static void addOneOwnerIntoGroup(String memberId, String groupId) {
        try {
            DirectoryObject directoryObject = new DirectoryObject();
            directoryObject.id = memberId;

            graphClient.groups(groupId).owners().references()
                    .buildRequest()
                    .post(directoryObject);
        } catch (Exception e) {
            System.out.println("Loi addOneOwnerIntoGroup()");
        }
    }

    public static void deleteOwnerGroup(String groupId, String ownerId) {
        graphClient.groups(groupId).owners(ownerId).reference()
                .buildRequest()
                .delete();
    }

    public static void deleteMemberGroup(String groupId, String memberId) {
        graphClient.groups(groupId).members(memberId).reference()
                .buildRequest()
                .delete();
    }


    public static void deleteGroup() {
        try {
            listGroups(0);
            System.out.print("Nhap groupId: ");
            String groupId = sc.nextLine();
            graphClient.groups(groupId)
                    .buildRequest()
                    .delete();
            System.out.println("Xoa thanh cong!");
        } catch (Exception e) {
            System.out.println("Xoa that bai!");
        }
    }

    public static void listDeletedGroup() {
        LinkedList<Option> requestOptions = new LinkedList<Option>();
        requestOptions.add(new HeaderOption("ConsistencyLevel", "eventual"));
        GroupCollectionPage groups = graphClient.directory().deletedItemsAsGroup()
                .buildRequest(requestOptions)
                .orderBy("displayName asc")
                .get();
        System.out.println("---------DANH SACH CAC NHOM 365 DA BI XOA-----------");
        int i = 1;
        for (Group g : groups.getCurrentPage()) {
            System.out.println("STT: " + i);
            System.out.println("Id: " + g.id);
            System.out.println("DisplayName: " + g.displayName);
            System.out.println("Description: " + g.description);
            System.out.println("GroupTypes: " + g.groupTypes);
            System.out.println("MailNickname: " + g.mailNickname);
            System.out.println("CreatedDateTime: " + getTime(g.createdDateTime));
            System.out.println("DeletedDateTime: " + getTime(g.deletedDateTime));
            System.out.println("Mail: " + g.mail);
            System.out.println("Privacy: " + g.visibility);
            i++;
        }
    }

    public static void restoreGroup() {
        try {
            listDeletedGroup();
            System.out.print("Nhap groupId: ");
            String groupId = sc.nextLine();
            graphClient.directory().deletedItems(groupId)
                    .restore()
                    .buildRequest()
                    .post();
            System.out.println("Khoi phuc thanh cong!");
        } catch (Exception e) {
            System.out.println("Khoi phuc that bai!");
        }
    }

    //Nhom 365 thi chi co user la duoc lam member
    //Nhom security co the user hooac nhom security khaclam thanh vien
    public static void listMembersofGroup(String groupId) {
        DirectoryObjectCollectionWithReferencesPage members = graphClient.groups(groupId).members()
                .buildRequest()
                .get();
        int i = 1;
        for (DirectoryObject d : members.getCurrentPage()) {
            if (d.oDataType.contains("user")) {
                System.out.println("Stt: " + i);
                getUser(d.id);
                i++;
            } else if (d.oDataType.contains("group")) {
                System.out.println("Stt: " + i);
                System.out.println("Name: " + getGroup(d.id).displayName);
                i++;
            }
        }
    }

    // danh cho group Security
    private static void getSecuInfoGroup(String groupId) {
        Group group = graphClient.groups(groupId)
                .buildRequest()
                .get();
        System.out.println("DisplayName: " + group.displayName);
    }

    private static Group getGroup(String groupId) {
        Group group = graphClient.groups(groupId)
                .buildRequest()
                .get();
        return group;
    }

    //cung chi co user la member cua nhom 365,security
    public static void listOwnersofGroup(String groupId) {
        DirectoryObjectCollectionWithReferencesPage members = graphClient.groups(groupId).owners()
                .buildRequest()
                .get();
        int i = 1;
        for (DirectoryObject d : members.getCurrentPage()) {
            System.out.println("Stt: " + i);
            getUser(d.id);
        }
    }

    public static void updateGroup() {
        try {
            //allowExternalSenders,autoSubscribeNewMembers,hideFromAddressLists
            //visibility
            //name, decription
            //owners, members ??... them vao thi de roi, xoa thi cung de nhung viet them ham lay danh sach member va owner.. Phai viet hai ham do
            // mail and aliases??.. chua co giai phap.. test thu moi biet
            listGroups(3);
            System.out.print("Nhap groupId muon chinh sua: ");
            String groupId = sc.nextLine();
            //Group oldGroup =getGroup(groupId); // sau nay se dung
            Group group = new Group();

            //--------General
            System.out.print("Name: ");
            group.displayName = sc.nextLine();
            System.out.print("Description: ");
            group.description = sc.nextLine();
            System.out.print("MailNickName(khong bao gom domain): ");
            group.mailNickname = sc.nextLine();

            //----------settings
            System.out.print("Privacy(Public/Private): ");
            group.visibility = sc.nextLine();
            Group group1 = new Group();
            System.out.print("Allow external senders to email this group(true/false): ");
            group1.allowExternalSenders = Boolean.valueOf(sc.nextLine());

            System.out.print("Send copies of group conversations and events to group members(true/false): ");
            group1.autoSubscribeNewMembers = Boolean.valueOf(sc.nextLine());

            System.out.print("Hide from my organization's global address list(true/false): ");
            group1.hideFromAddressLists = Boolean.valueOf(sc.nextLine());

        /*
        //---------Members
        System.out.println("List Owners:");
        listOwnersofGroup(groupId);
        System.out.println("List Members:");
        listMembersofGroup(groupId);

        //---Them owners
        String tmp="";
        System.out.print("Ban co muon them Owner(Yes/No): ");
        tmp=sc.nextLine();
        if(tmp.equalsIgnoreCase("yes")){
            //

        }
        //---Them members
        System.out.print("Ban co muon them Member(Yes/No): ");
        tmp=sc.nextLine();
        if(tmp.equalsIgnoreCase("yes")){
            //
        }
        //---Xoa Owners
        System.out.print("Ban co muon xoa Owner(Yes/No): ");
        tmp=sc.nextLine();
        if(tmp.equalsIgnoreCase("yes")){
            //
        }
        //---Xoa member
        System.out.print("Ban co muon xoa Member(Yes/No): ");
        tmp=sc.nextLine();
        if(tmp.equalsIgnoreCase("yes")){
            //
        }
         */

            graphClient.groups(groupId)
                    .buildRequest()
                    .patch(group);
            graphClient.groups(groupId)
                    .buildRequest()
                    .patch(group1);
            System.out.println("Cap nhat thanh cong!");

        } catch (Exception e) {
            System.out.println("Cap nhat that bai!");
        }

    }

    public static void updateGroupSecurity() {
        try {
            listGroups(2);
            System.out.print("Nhap groupId muon chinh sua: ");
            String groupId = sc.nextLine();

            Group group = new Group();
            System.out.print("Name: ");
            group.displayName = sc.nextLine();
            System.out.print("Description: ");
            group.description = sc.nextLine();
            graphClient.groups(groupId)
                    .buildRequest()
                    .patch(group);
            System.out.println("Cap nhat thanh cong!");
        } catch (Exception e) {
            System.out.println("Cap nhat that bai!");
        }
    }

    public static boolean validateMailNickname(String type) {
        String entityType = type;
        //String entityType = "Group";
        //String displayName = "Myprefix_test_mysuffix";
        System.out.print("Nhap mailNickname: ");
        String mailNickname = sc.nextLine();
        UUID onBehalfOfUserId = UUID.fromString(getMe().id);
        try {
            graphClient.directoryObjects()
                    .validateProperties(DirectoryObjectValidatePropertiesParameterSet
                            .newBuilder()
                            .withEntityType(entityType)
                            //.withDisplayName(displayName)
                            .withMailNickname(mailNickname)
                            .withOnBehalfOfUserId(onBehalfOfUserId)
                            .build())
                    .buildRequest()
                    .post();
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    //----------------------CONTACT--------------
    public static void createContact() {
        try {
            Contact contact = new Contact();
            System.out.print("First name: ");
            contact.givenName = sc.nextLine();
            System.out.print("Last name: ");
            contact.surname = sc.nextLine();

            //displayName phai duy nhat
            System.out.print("Display name: ");
            contact.displayName = sc.nextLine();


            //System.out.print("Hide from organization's global address list(true/false): ");

            //Chung thuc email co trung voi cac mail group.(chua lam)
            LinkedList<EmailAddress> emailAddressesList = new LinkedList<EmailAddress>();
            EmailAddress emailAddresses = new EmailAddress();

            String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                    + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
            //assertTrue(EmailValidation.patternMatches(emailAddress, regexPattern));
            do {
                System.out.print("Email: ");
                emailAddresses.address = sc.nextLine();
            }while (!Pattern.matches(regexPattern, emailAddresses.address));

            emailAddresses.name = contact.displayName;
            emailAddressesList.add(emailAddresses);
            contact.emailAddresses = emailAddressesList;

            System.out.print("Mail tip: ");
            contact.personalNotes = sc.nextLine();

            LinkedList<String> businessPhonesList = new LinkedList<String>();
            System.out.print("Phone: ");
            String phone = sc.nextLine();
            businessPhonesList.add(phone);
            contact.businessPhones = businessPhonesList;

            graphClient.me().contacts()
                    .buildRequest()
                    .post(contact);
            System.out.println("Tao thanh cong!");
        }catch (Exception e){
            System.out.println("Tao that bai!");
        }
    }

    public static void listContact() {
        ContactCollectionPage contacts = graphClient.me().contacts()
                .buildRequest()
                .get();
        int i=1;
        System.out.println("---------CONTACT---------");
        for (Contact c : contacts.getCurrentPage()) {
            System.out.println("STT: "+i);
            System.out.println("Id: "+c.id);
            System.out.println("ParentFolderId: "+c.parentFolderId);
            System.out.println("Name: "+c.displayName);
            System.out.print("Email: ");
            c.emailAddresses.forEach((element) -> {
                System.out.println(element.address + " ");
            });
            i++;
            System.out.println("---------------------------------");

            //System.out.println("Email: "+c.emailAddresses.toString());
        }
    }
    /*
    public static void listContactUser() {

        getUsers();
        String userId;
        try {
            System.out.print("Nhap userId: ");
            userId = sc.nextLine();
            ContactCollectionPage contacts = graphClient.users(userId).contacts()
                    .buildRequest()
                    .get();
            int i = 1;
            System.out.println("---------CONTACT---------");
            for (Contact c : contacts.getCurrentPage()) {
                System.out.println("STT: " + i);
                System.out.println("Id: " + c.id);
                System.out.println("ParentFolderId: " + c.parentFolderId);
                System.out.println("Name: " + c.displayName);
                System.out.print("Email: ");
                c.emailAddresses.forEach((element) -> {
                    System.out.println(element.address + " ");
                });
                i++;
                System.out.println("---------------------------------");
            }
        }catch (Exception e){
            System.out.println("Lay danh sach user that bai!");
        }
    }
     */
    public static void deleteContact() {
        listContact();
        try {
            System.out.print("Nhap id: ");
            String contactId = sc.nextLine();
            graphClient.me().contacts(contactId)
                    .buildRequest()
                    .delete();
            System.out.println("Xoa thanh cong!");
        }catch (Exception r){
            System.out.println("Xoa that bai!");
        }

    }

    public static void updateContact() {
        listContact();
        System.out.println("-------UPDATE--------");
        try {
            System.out.print("Nhap id contact muon chinh sua: ");
            String id = sc.nextLine();

            Contact contact = new Contact();
            System.out.print("First name: ");
            contact.givenName = sc.nextLine();
            System.out.print("Last name: ");
            contact.surname = sc.nextLine();

            //displayName phai duy nhat
            System.out.print("Display name: ");
            contact.displayName = sc.nextLine();

            //Chung thuc email co trung voi cac mail group.(chua lam)
            LinkedList<EmailAddress> emailAddressesList = new LinkedList<EmailAddress>();
            EmailAddress emailAddresses = new EmailAddress();

            String regexPattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@"
                    + "[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
            //assertTrue(EmailValidation.patternMatches(emailAddress, regexPattern));
            do {
                System.out.print("Email: ");
                emailAddresses.address = sc.nextLine();
            }while (!Pattern.matches(regexPattern, emailAddresses.address));

            emailAddresses.name = contact.displayName;
            emailAddressesList.add(emailAddresses);
            contact.emailAddresses = emailAddressesList;

            System.out.print("Mail tip: ");
            contact.personalNotes = sc.nextLine();
            System.out.print("Company: ");
            contact.companyName = sc.nextLine();
            System.out.print("Mobile phone: ");
            contact.mobilePhone = sc.nextLine();
            System.out.print("Title: ");
            contact.title = sc.nextLine();
            System.out.print("Website: ");
            contact.businessHomePage = sc.nextLine();


            PhysicalAddress businessAddress = new PhysicalAddress();
            System.out.print("City: ");
            businessAddress.city=sc.nextLine();
            System.out.print("Country: ");
            businessAddress.countryOrRegion=sc.nextLine();
            System.out.print("PostalCode: ");
            businessAddress.postalCode=sc.nextLine();
            System.out.print("State: ");
            businessAddress.state=sc.nextLine();
            System.out.print("Street: ");
            businessAddress.street=sc.nextLine();
            contact.businessAddress=businessAddress;
            System.out.print("Fax code: ");
            String imAddresses=sc.nextLine();
            List<String> fax=Arrays.asList(imAddresses);
            contact.imAddresses=fax;
            LinkedList<String> businessPhonesList = new LinkedList<String>();
            System.out.print("Phone: ");
            String phone = sc.nextLine();
            businessPhonesList.add(phone);
            contact.businessPhones = businessPhonesList;

            graphClient.me().contacts(id)
                    .buildRequest()
                    .patch(contact);

            System.out.println("Chinh sua thanh cong!");
        }catch (Exception e){
            System.out.println("Chinh sua that bai!");
        }
    }


    public static String random() {
        Random generator = new Random();
        //int value = generator.nextInt(4) + 1;
        //int value = generator.nextInt((max - min) + 1) + min;
        int code = (int) Math.floor(((Math.random() * 8999) + 1000));
        return String.valueOf(code);
    }
}



