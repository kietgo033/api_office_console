package com.example.office.api_office;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiOfficeApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiOfficeApplication.class, args);
    }

}
